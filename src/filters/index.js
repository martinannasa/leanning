import Vue from 'vue'
// 导入过滤器
import * as filters from './filters'
// 批量注册过滤器
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})
