import dayjs from 'dayjs'

// 格式化时间
export const formatTime = (val, str = 'YYYY-MM-DD') => {
  return dayjs(val).format(str)
}

// 显示试题类型
export const showQuestType = (val) => {
  if (val === 1) return '单选题'
  if (val === 2) return '多选题'
  if (val === 3) return '判断题'
  if (val === 4) return '简单题'
}
// 显示试题类型
export const showQuestShowState = (val) => {
  if (val === 1) return '单选题'
  if (val === 2) return '多选题'
  if (val === 3) return '判断题'
  if (val === 4) return '简单题'
}
