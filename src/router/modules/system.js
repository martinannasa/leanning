import Layout from '@/layout'
export default {
  path: '/system',
  component: Layout,
  name: 'system',
  meta: { title: '系统设置', icon: 'table' },
  children: [
    {
      path: 'config',
      component: () => import('@/views/system/config/index.vue'),
      meta: {
        title: '系统配置',
        icon: 'table'
      }
    },
    {
      path: 'loginlog',
      component: () => import('@/views/system/loginlog/index.vue'),
      meta: {
        title: '登录日志',
        icon: 'table'
      }
    }
  ]
}
