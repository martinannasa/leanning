import Layout from '@/layout'
export default {
  path: '/subject',
  component: Layout,
  name: 'subject',
  meta: { title: '学科管理', icon: 'table' },
  children: [
    {
      path: 'subject',
      component: () => import('@/views/subject/subject/index.vue'),
      meta: {
        title: '学科信息',
        icon: 'table'
      }
    },
    {
      path: 'directory',
      component: () => import('@/views/subject/directory/index.vue'),
      meta: {
        title: '目录信息',
        icon: 'table'
      }
    },
    {
      path: 'tag',
      component: () => import('@/views/subject/tag/index.vue'),
      meta: {
        title: '标签信息',
        icon: 'table'
      }
    }
  ]
}
