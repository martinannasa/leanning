import Layout from '@/layout'
export default {
  path: '/permission',
  component: Layout,
  name: 'permission',
  meta: { title: '权限管理', icon: 'table' },
  children: [
    {
      path: 'menu',
      component: () => import('@/views/permission/menu/index.vue'),
      meta: {
        title: '菜单管理',
        icon: 'table'
      }
    },
    {
      path: 'role',
      component: () => import('@/views/permission/role/index.vue'),
      meta: {
        title: '角色管理',
        icon: 'table'
      }
    },
    {
      path: 'user',
      component: () => import('@/views/permission/user/index.vue'),
      meta: {
        title: '用户管理',
        icon: 'table'
      }
    }
  ]
}
