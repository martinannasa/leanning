import QuestionList from './QuestionList'
import LookTest from './LookTest'
export default {
  install(Vue) {
    Vue.component('QuestionList', QuestionList)
    Vue.component('LookTest', LookTest)
  }
}
