import store from '@/store'

// 相同的配置项
export default {
  methods: {
    // point 某个按钮对应的权限标识
    checkPermission(point) {
      // 判断按钮的权限标识否在 该用户的所有按钮权限标识数组中
      // const data = await store.dispatch('user/getInfo')
      const btnData = store.getters.btnData
      // console.log(btnData)
      if (btnData.length) {
        return btnData.some(item => item.permission_mark === point)
      }
      return false
    }
  }
}
