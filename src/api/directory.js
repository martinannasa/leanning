import request from '@/utils/request'

export function getSubject() {
  return request({
    url: `/subject/tree`,
    method: 'get'
  })
}
// 筛选
export function getdirectory(params) {
  return request({
    url: `/subject/directory`,
    method: 'get',
    params
  })
}
// 获取目录列表
export function getDirectoryList(params) {
  return request({
    url: `/subject/directory`,
    method: 'get',
    params
  })
}
// 删除
export function deleteDirectory(id) {
  return request({
    url: `/subject/directory/${id}`,
    method: 'DELETE'
  })
}

// 增加
export function addDirectory(data) {
  return request({
    url: `/subject/directory/`,
    // url: '/subject/subject/',
    method: 'post',
    data
  })
}
// 编辑的回显
export function getSmipleDirectoryList(id) {
  return request({
    url: `/subject/directory/${id}`,
    method: 'get'
  })
}
// 进行修改
export function editDirectory(data) {
  return request({
    url: `/subject/directory/${data.subject_id}`,
    method: 'put',
    data
  })
}
