import request from '@/utils/request'

export const getRolesList = params => {
  return request({
    url: '/permission/role',
    method: 'GET',
    params
  })
}

export const getRole = id => {
  return request({
    url: `/permission/role/${id}`,
    method: 'GET'

  })
}

export const addRolesList = data => {
  return request({
    url: '/permission/role',
    method: 'POST',
    data
  })
}

export const deleteRolesList = id => {
  return request({
    url: `/permission/role/${id}`,
    method: 'delete'
  })
}

export const editRolesList = data => {
  return request({
    url: `/permission/role/${data.id}`,
    method: 'put',
    data
  })
}
