// 考试模块所有请求
import request from '@/utils/request'
export function getTreeList() {
  return request({
    url:  "/subject/tree",
  })
}
export function getPaperList(params) {
  return request({
    url:  "/exam/paper",
    params
  })
}
export function changePaperStatus(state,id) {
  return request({
    url:  `exam/paper/${state}/${id}`,
  })
}export function deletePaper(id) {
  return request({
    url:  `/exam/paper/${id}`,
    method: 'DELETE',
  })
}
export function postPaper(data){
  return request({
    url:'/exam/paper',
    method:'POST',
    data
  })
}

export function editPaper(id){
  return request({
    url:`/exam/paper/${id}`,
  })
}

export function realEditPaper(data){
  return request({
    url:`/exam/paper/${data.id}`,
    method:'PUT',
    data
  })
}