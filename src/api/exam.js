// 考试模块所有请求
import request from '@/utils/request'
export function getTreeList() {
  return request({
    url: '/subject/tree'
  })
}
export function getQuestionList(params) {
  return request({
    url: '/exam/question',
    params
  })
}
export function editQuestionShow(data) {
  return request({
    url: '/exam/question/show',
    method: 'POST',
    data
  })
}
export function editQuestionCheck(data) {
  return request({
    url: '/exam/question/check',
    method: 'POST',
    data
  })
} export function deleteQuestion(id) {
  return request({
    url: `/exam/question/${id}`,
    method: 'DELETE'
  })
}
export const getQuestionInfo = id => {
  return request({
    url: '/exam/question/' + id,
    method: 'GET'
  })
}
export const addQuestion = data => {
  return request({
    url: '/exam/question',
    method: 'POST',
    data
  })
}
export const editQuestion = data => {
  return request({
    url: '/exam/question/' + data.id,
    method: 'PUT',
    data
  })
}
