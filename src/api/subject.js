import request from '@/utils/request'

// 获取标签列表
export function getSubjectTag(params) {
  return request({
    url: '/subject/tag',
    params
  })
}

// 创建标签
export function addSubjectTag(data) {
  return request({
    method: 'POST',
    url: '/subject/tag',
    data
  })
}

// 删除标签
export function deleteSubjectTag(id) {
  return request({
    method: 'DELETE',
    url: `/subject/tag/${id}`
  })
}

// 获取学科信息
export function getSubjectName() {
  return request({
    url: '/subject/tree'
  })
}

// 编辑标签
export function editSubjectTag(data) {
  return request({
    method: 'PUT',
    url: `/subject/tag/${data.id}`,
    data
  })
}

// 数据回显
export function echoSubject(id) {
  return request({
    url: `/subject/tag/${id}`
  })
}

// 修改数据
// export function editSubjectInfo(id) {
//   return request({
//     url: `/subject/tag/${id}`,
//     method: 'PUT'
//   })
// }

// 切换状态
export function switchStatus(data) {
  return request({
    url: `/subject/tag/${data.id}`,
    method: 'PUT',
    data
  })
}

export function getSubjectList(params) {
  return request({
    url: `/subject/subject/`,
    method: 'get',
    params
  })
}
// 编辑的回显
export function getSmipleSubjectList(id) {
  return request({
    url: `/subject/subject/${id}`,
    method: 'get'
  })
}

export function getlist(params) {
  return request({
    url: `/subject/subject/`,
    method: 'get',
    params
  })
}
export function deleteSubject(id) {
  return request({
    url: `/subject/subject/${id}`,
    method: 'DELETE'
  })
}

export function addSubject(data) {
  return request({
    url: `/subject/subject/`,
    // url: '/subject/subject/',
    method: 'post',
    data
  })
}

export function editSubject(data) {
  return request({
    url: `/subject/subject/${data.id}`,
    method: 'put',
    data
  })
}

export function switchsubject(data) {
  return request({
    url: `/subject/subject/${data.id}`,
    method: 'put',
    data
  })
}

