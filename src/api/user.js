import request from '@/utils/request'

export function login(data) {
  return request({
    // url: '/vue-admin-template/user/login',
    url: '/index/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/index/userinfo',
    method: 'post',
    params: { token }
  })
}

// export function logout() {
//   return request({
//     url: '/index/logout',
//     method: 'post'
//   })
// }

export const fn = data => {
  return request({
    url: '/index/login',
    method: 'POST',
    data
  })
}
// permission -------------user----------------------------------
// permission -------------user----------------------------------
// permission -------------user----------------------------------
export function getUserList(params) {
  return request({
    url: `permission/user`,
    method: 'get',
    params
  })
}
// 删除
export function deleteuser(id) {
  return request({
    url: `permission/user/${id}`,
    method: 'DELETE'
  })
}
//
export function getsimpleuser(id) {
  return request({
    url: `permission/user/${id}`,
    method: 'get'
  })
}
// 添加
export function adduser(data) {
  return request({
    url: `permission/user`,
    method: 'post',
    data
  })
}

// 获取弹框里面的角色列表
export function getusercheckbox() {
  return request({
    url: `permission/role`,
    method: 'get'
  })
}

// 修改
export function edituser(data) {
  return request({
    url: `permission/user/${data.id}`,
    method: 'put',
    data
  })
}

export function pic(data) {
  return request({
    url: `index/upload?mode=base64&type=image`,
    method: 'post',
    data
  })
}

// switch 切换
export function switchuser(params) {
  return request({
    url: `/permission/user/${params.id}/${params.status}`,
    method: 'get'
  })
}

