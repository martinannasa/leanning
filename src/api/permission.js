import request from '@/utils/request'

// 获取菜单权限
export const getMenuList = () => {
  return request({
    url: '/permission/menu'
  })
}

// 添加菜单权限
export const addMenuList = (data) => {
  return request({
    url: '/permission/menu',
    method: 'post',
    data
  })
}

// 编辑菜单权限
export const editMenuList = (data) => {
  return request({
    url: `/permission/menu/${data.id}`,
    method: 'put',
    data
  })
}

// 删除菜单权限
export const deleteMenuList = (id) => {
  return request({
    url: `/permission/menu/${id}`,
    method: 'delete'
  })
}

// switch 切换
export function switchmenu(params) {
  return request({
    url: `/permission/menu/${params.id}/${params.status}`,
    method: 'get'
  })
}
