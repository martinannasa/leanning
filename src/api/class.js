// 班级模块所有请求
import request from '@/utils/request'
export function getClass(params) {
  return request({
    url: '/class/class',
    method: 'get',
    params

  })
}
export function AddClass(data) {
  return request({
    url: '/class/class',
    method: 'post',
    data
  })
}
export function delClass(id) {
  return request({
    url: `/class/class/${id}`,
    method: 'DELETE'
  })
}
export function editClass(data) {
  return request({
    url: `/class/class/${data.id}`,
    method: 'PUT',
    data
  })
}
export function subJect() {
  return request({
    url: '/subject/tree',
    method: 'GET'
  })
}
export function postStatus(data) {
  return request({
    url: `/class/class/${data.id}`,
    method: 'PUT',
    data
  })
}
export function putClass(id) {
  return request({
    url: `/class/class/${id}`,
    method: 'get'
  })
}
export function teacherClass(params) {
  return request({
    url: '/class/teacher',
    method: 'get',
    params
  })
}

export function zhujiaoClass(params) {
  return request({
    url: '/class/teacher',
    method: 'get',
    params
  })
}
export function managerClass(params) {
  return request({
    url: '/class/teacher',
    method: 'get',
    params
  })
}
export function amendClass(id) {
  return request({
    url: `/class/class/${id}`,
    method: 'get'
  })
}
// 学生信息模块
//
export function getStudentList(params) {
  return request({
    method: 'GET',
    url: '/class/student',
    params
  })
}

// 归属学科下拉菜单获取信息
export function getStudent(params) {
  return request({
    method: 'GET',
    url: '/subject/tree',
    params
  })
}
// 归属班级下拉信息
export function getStudentClass(params) {
  return request({
    method: 'GET',
    url: '/class/class',
    params
  })
}
// 删除操作
export function deletStudentClass(data) {
  return request({
    method: 'DELETE',
    url: 'class/student',
    data
  })
}
// 增加学员
export function addStudentClass(data) {
  return request({
    method: 'POST',
    url: 'class/student',
    data
  })
}
// 数据回显
export const changeStudent = id => {
  return request({
    url: `/class/student/${id}`
  })
}

// 更新用户数据
export const fn = data => {
  return request({
    url: `/class/student/${data.id}`,
    method: 'PUT',
    data
  })
}

// 导入数据
export const daoru = list => {
  return request({
    url: 'class/student/import',
    method: 'POST',
    data: {
      list
    }
  })
}
// 单选框的显示和隐藏
export function batchEnable(data) {
  return request({
    method: 'put',
    url: '/class/studentChangeStatus',
    data
  })
}
