import store from '@/store'
// import { on } from 'element-ui/src/utils/dom'
import { Message } from 'element-ui'

// 指令名 = 指令内容对象

// 图片加载失败受损或者没有图，用占位功能
export const imgpix = {
  inserted(el, binding) {
    if (el.src) {
      el.onerror = function() {
        // 当图片出现异常的时候 会将指令配置的默认图片设置为该图片的内容
        el.src = binding.value // 这里不能写死
      }
    } else {
      // 如果没有配置src，且使用该指令可以实现默认图
      el.src = binding.value
    }
  }
}
// 按钮权限指令
export const permission = {
  bind(el, binding, vnode) {
    const mark = binding.value
    if (!mark) {
      return new Error('请输入权限标识')
    }
    const idx = store.getters.permission_btn_list.findIndex(val => val.permission_mark === mark)
    if (idx > -1) {
      return
    }
    // 禁用 click 兼容性检测
    if (!vnode.componentInstance) {
      alert('v-permission 不支持用在普通dom元素')
      return
    }
    if (vnode.data.nativeOn && vnode.data.nativeOn.click) {
      alert('v-permission 不支持 click.native')
      return
    }
    // 禁用点击事件(组件）
    if (vnode.componentInstance) {
      vnode.componentInstance.$off('click')
    }
    el.onclick = function(e) {
      e.preventDefault()
      e.stopPropagation()
      Message.error('您无权进行此操作')
    }
  }
}

export const imageerror = {
  inserted(dom, options) {
    //   图片异常的逻辑
    //  监听img标签的错误事件  因为图片加载失败 会触发  onerror事件
    dom.src = dom.src || options.value

    dom.onerror = function() {
      // 图片失败  赋值一个默认的图片
      dom.src = options.value
    }
  },
  componentUpdated(dom, options) {
    dom.src = dom.src || options.value
  }
}
