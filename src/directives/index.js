import Vue from 'vue'

// 导入所有指令
import * as directives from './directives'

// 批量注册指令
Object.keys(directives).forEach(key => {
  Vue.directive(key, directives[key])
})
