const getters = {
  sidebar: state => state.app.sidebar,
  size: state => state.app.size,
  device: state => state.app.device,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  token: state => state.user.token,
  avatar: state => state.user.userInfo.avatar,
  name: state => state.user.userInfo.username,
  introduction: state => state.user.introduction,
  roles: state => state.user.roles,
  routes: state => state.permission.routes, // 完整的路由映射表 (每个人都不一样)
  errorLogs: state => state.errorLog.logs,
  btnData: state => state.user.checkPermissionBtn
}
export default getters
