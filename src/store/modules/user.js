import { login, getInfo } from '@/api/user'
import { getToken, setToken, removeToken, setTimeStamp } from '@/utils/auth'
import { resetRouter } from '@/router'

const getDefaultState = () => {
  return {
    token: getToken(),
    // name: '',
    // avatar: '',
    userInfo: {},
    checkPermissionBtn: []
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_USERINFO: (state, data) => {
    state.userInfo = data
  },
  removeToken: (state) => {
    state.token = null
    removeToken()
  },
  removeUserInfo: (state) => {
    state.userInfo = {}
  },
  SET_BTN: (state, data) => {
    state.checkPermissionBtn = data
  }
}

const actions = {
  // 用户登录
  login({ commit }, userInfo) {
    return new Promise((resolve, reject) => {
      login(userInfo)
        .then((response) => {
          const { data } = response
          console.log(data)
          // 这里需要调整，读取token的深浅不一致
          // 触发mutations
          commit('SET_TOKEN', data.token)
          // 存储到本地
          setToken(data.token)
          setTimeStamp()
          resolve()
        })
        .catch((error) => {
          console.log(error)
          reject(error)
        })
    })
  },

  // 获取用户资料
  async getInfo({ commit }, state) {
    const { data } = await getInfo(state)
    console.log(data)
    // commit('SET_NAME',data.userinfo.username)
    // commit('SET_AVATAR',data.userinfo.avatar)
    commit('SET_USERINFO', data.userinfo)
    commit('SET_BTN', data.permission_btn_list)
    return data
    // return new Promise((resolve, reject) => {
    //   getInfo(state.token).then(response => {
    //     const { data } = response

    //     if (!data) {
    //       return reject('Verification failed, please Login again.')
    //     }
    //     // 读取的字段可能不一致
    //     const { name, avatar } = data
    //     // 设置给state
    //     commit('SET_NAME', name)
    //     commit('SET_AVATAR', avatar)
    //     resolve(data)
    //   }).catch(error => {
    //     reject(error)
    //   })
    // })
  },

  // 退出操作
  logout({ commit }, state) {
    commit('removeToken')
    commit('removeUserInfo')
    resetRouter()
    commit('permission/setRoutes', [], { root: true })
  }

  // // 退出操作
  // logout({ commit, state }) {
  //   return new Promise((resolve, reject) => {
  //     logout(state.token)
  //       .then(() => {
  //         // 移除token，先移除token
  //         commit('removeToken')
  //         // 重置路由
  //         // resetRouter();
  //         // 重置state数据
  //         commit('removeUserInfo')
  //         resolve();
  //       })
  //       .catch((error) => {
  //         reject(error);
  //       });
  //   });
  // },

  // // 移除token
  // resetToken({ commit }) {
  //   return new Promise((resolve) => {
  //     // 先移除token
  //     removeToken();
  //     // 重置state数据
  //     commit("RESET_STATE");
  //     resolve();
  //   });
  // },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
