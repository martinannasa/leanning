import { getTreeList } from '@/api/exam/'
const state ={
  treeList:[]
}
const getters={
  treeList:state=>state.treeList
}
const mutations = {
 setTreeList(state,data){
   state.treeList=data
 }

 }

const actions={
 async loadTreeList({ commit },data){
   data=await getTreeList()
  commit('setTreeList', data)
  }
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
