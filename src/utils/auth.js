import Cookies from 'js-cookie'
import { getStorage, setStorage, removeStorage } from '@/utils'
// 建议自己重新写一个名称！
const TokenKey = 'vue-admin'
const TimeKey = 'vue-time'
const RefreshTokenKey = 'refresh-token'
export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  setStorage(RefreshTokenKey, token)
  return Cookies.set(TokenKey, token)
 
}

export function removeToken() {
  removeStorage(RefreshTokenKey)
  return Cookies.remove(TokenKey)

}
// 读取刷新token
export function getRefreshToken() {
  return getStorage(RefreshTokenKey)
}
export function getTimeStamp() {
  return Cookies.get(TimeKey)
}

export function setTimeStamp() {
  return Cookies.set(TimeKey, Date.now())
}
